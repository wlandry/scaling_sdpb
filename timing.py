#!/usr/bin/env python3

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

openmp_ising_cores = [4,12,28]
openmp_ising_times = [3596,1272,756]
openmp_ising_scaled_times = [a*b/3600 for a,b in zip(openmp_ising_cores,openmp_ising_times)]
openmp_O2_cores = [28]
openmp_O2_times = [7982]
openmp_O2_scaled_times = [a*b/3600 for a,b in zip(openmp_O2_cores,openmp_O2_times)]

elemental_ising_cores= [4, 12, 28, 2*28, 4*28, 8*28, 16*28]
elemental_ising_times = [2934, 993, 445, 258, 142, 77, 57]
elemental_ising_scaled_times = [a*b/3600 for a,b in zip(elemental_ising_cores,elemental_ising_times)]
elemental_O2_cores= [2*28, 4*28, 8*28, 16*28]
elemental_O2_times = [1188,799,400,188]
elemental_O2_scaled_times = [a*b/3600 for a,b in zip(elemental_O2_cores,elemental_O2_times)]

plt.figure(figsize=(12,9))

# plt.ylim(0,24000/3600)

plt.plot(openmp_ising_cores,openmp_ising_scaled_times,'o--',label="Ising: OpenMP",linewidth=4,markersize=10)
plt.plot(elemental_ising_cores,elemental_ising_scaled_times,'s:',label="Ising: MPI",linewidth=4,markersize=10)

plt.plot(openmp_O2_cores,openmp_O2_scaled_times,'*--',label="O(2): OpenMP",linewidth=4,markersize=10)
plt.plot(elemental_O2_cores,elemental_O2_scaled_times,'D:',label="O(2): MPI",linewidth=4,markersize=10)

plt.ylabel('Time (hours) x # Cores',fontsize='xx-large')
plt.xlabel('# Cores',fontsize='xx-large')
plt.title('Total Core-Hours',fontsize='xx-large')
plt.xticks(size=15)
plt.yticks(size=15)

plt.legend(fontsize='xx-large')

plt.savefig("timing.png",dpi=300)
