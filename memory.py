#!/usr/bin/env python3

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

openmp_ising_cores = [4,12,28]
openmp_ising_memory = [30849480, 31390316, 32471724]
openmp_ising_scaled_memory = [a/(1024*1024) for a in openmp_ising_memory]
openmp_O2_cores = [28]
openmp_O2_memory = [179963132]
openmp_O2_scaled_memory = [a/(1024*1024) for a in openmp_O2_memory]

elemental_ising_cores= [4, 12, 28, 2*28, 4*28, 8*28, 16*28]
elemental_ising_memory = [30728264, 44338316, 73511408, 61940044, 53375864, 49304296, 48611788]
elemental_ising_scaled_memory = [a/(1024*1024) for a in elemental_ising_memory]
elemental_O2_cores= [28, 2*28, 4*28, 8*28, 16*28]
elemental_O2_memory = [226146100, 167650200, 122542512, 96419452, 87854136]
elemental_O2_initial_memory = [201885880, 145193564, 119537120, 118628108, 73336304]
elemental_O2_scaled_memory = [a/(1024*1024) for a in elemental_O2_memory]

plt.figure(figsize=(12,9))

plt.ylim(0,250)

plt.plot(openmp_ising_cores,openmp_ising_scaled_memory,'o--',label="Ising: OpenMP",linewidth=4,markersize=10)
plt.plot(elemental_ising_cores,elemental_ising_scaled_memory,'s:',label="Ising: MPI",linewidth=4,markersize=10)

plt.plot(openmp_O2_cores,openmp_O2_scaled_memory,'*--',label="O(2): OpenMP",linewidth=4,markersize=10)
plt.plot(elemental_O2_cores,elemental_O2_scaled_memory,'D:',label="O(2): MPI",linewidth=4,markersize=10)

plt.ylabel('Max Memory (GiB)/Node',fontsize=30)
plt.xlabel('# Cores',fontsize=30)
# plt.title('Memory Use',fontsize=32)
plt.xticks(size=30)
plt.yticks(size=30)

plt.legend(fontsize=30)

plt.savefig("memory.png",dpi=300)
