#!/usr/bin/env python3

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

openmp_ising_cores = [4,12,28]
openmp_ising_times = [3568, 1517, 732]
openmp_ising_scaled_times = [a/3600 for a in openmp_ising_times]
openmp_O2_cores = [28]
openmp_O2_times = [7950]
openmp_O2_scaled_times = [a/3600 for a in openmp_O2_times]

elemental_ising_cores= [4, 12, 28, 2*28, 4*28, 8*28, 16*28]
elemental_ising_times = [2861, 982, 444, 261, 146, 82, 59]
# 16*28 time is variable?: 60, 64, 66
# 8*28 time is variable?: 83
# elemental_ising_times = [2934, 993, 445, 258, 142, 77, 57]
elemental_ising_scaled_times = [a/3600 for a in elemental_ising_times]
elemental_O2_cores= [28, 2*28, 4*28, 8*28, 16*28]
elemental_O2_times = [2254, 1190, 824, 440, 281]
# 8*28 times: 443, 470
# 16*28 times: 412, 337, 408 :(
# elemental_O2_cores= [2*28, 4*28, 8*28, 16*28]
# elemental_O2_times = [1188,799,400,188] MPI_Reduce_scatter
# elemental_O2_times = [,,403,] MPI_Reduce_scatter
elemental_O2_scaled_times = [a/3600 for a in elemental_O2_times]

fig, ax = plt.subplots()

# plt.figure(figsize=(12,9))

ax.plot(openmp_ising_cores,openmp_ising_times,'o--',label="Ising: OpenMP")
ax.plot(elemental_ising_cores,elemental_ising_times,'s:',label="Ising: MPI")

ax.plot(openmp_O2_cores,openmp_O2_times,'*--',label="O(2): OpenMP")
ax.plot(elemental_O2_cores,elemental_O2_times,'D:',label="O(2): MPI")


# ax.plot(openmp_ising_cores,openmp_ising_scaled_times,'o--',label="Ising: OpenMP")
# ax.plot(elemental_ising_cores,elemental_ising_scaled_times,'s:',label="Ising: MPI")

# ax.plot(openmp_O2_cores,openmp_O2_scaled_times,'*--',label="O(2): OpenMP")
# ax.plot(elemental_O2_cores,elemental_O2_scaled_times,'D:',label="O(2): MPI")

ax.set_xscale('log')
ax.set_yscale('log')

# ax.ticklabel_format(style='sci')

ax.set_ylabel('Time (s)', fontsize=15)
ax.set_xlabel('# Cores', fontsize=15)
ax.set_title('Time for Second Iteration',fontsize=18)

ax.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.0f'))
# ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0f'))

ax.legend(loc=3, fontsize=14)

plt.xticks(size=15)
plt.yticks(size=15)


plt.savefig("timing_log.png",dpi=300)
